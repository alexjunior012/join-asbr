#Venha trabalhar na ActualSales!

##A Empresa

A ActualSales é uma empresa de marketing digital focada em geração de leads através de Landing Pages.
Relaxa, nós também não sabíamos o que isso significa ;)

##Vagas

###Developer Full Stack
No momento estamos reestruturando o departamento de TI e precisamos de um programador experiente para **passar por um breve período de adaptação e 
assumir a liderança da equipe de desenvolvimento web em seguida**. Topa?

####Requisitos:

- Vivência em Desenvolvimento Web, preferencialmente PHP
- Conseguir se virar em qualquer situação que surja (no escopo de TI)
- Sentir-se à vontade para ajudar e orientar os demais membros da equipe
- Facilidade para lidar com clientes internos (gerentes de contas)
- Querer vivenciar a gestão de uma equipe de TI
- Conhecimento e vontade (principalmente) de aplicar boas práticas e metodologias

####Bônus:
- Interesse por infra e ops

####Nosso stack atual:
- PHP (framework baseado no Zend)
- MySQL
- Git (Bitbucket com deploy contínuo)
- Jquery
- Bootstrap
- Apache
- Varnish
- **Importante ressaltar que esse stack está sempre aberto para novas tecnologias**

####Oferecemos:
- Ambiente informal (chinelo e breja no trabalho é OK)
- (VR || VA) && VT
- Plano de saúde e dental

Faixa salarial: 5k

####Como me candidatar?
Não queremos ver o seu CV, mas sim o seu código.
Para isso, elaboramos um pequeno teste de aptidões que pode ser realizado no seu tempo.
As instruções para execução desse teste estão localizadas na pasta landing-page desse mesmo repositório.

####Dúvidas
Para dúvidas ou mais informações, fique à vontade para nos contactar através do email <vagas-ti@actualsales.com.br>.


Obrigado e boa sorte!